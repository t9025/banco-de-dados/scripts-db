create database db_bs_authenticate;

comment on database db_bs_authenticate is 'Database Boa Saude Authenticate';


create table acc_account
(
    acc_id           serial
        constraint acc_account_pk
            primary key,
    acc_username     varchar(255) not null,
    acc_password     varchar(255) not null,
    acc_first_name   varchar(255) not null,
    acc_last_name  varchar(255) not null,
    acc_enable     bool         not null,
    acc_created_at date         not null,
    acc_created_by varchar(255) not null,
    acc_modified_at  date,
    acc_modified_by  varchar(255)
);

create unique index acc_account_acc_id_uindex
    on acc_account (acc_id);

create unique index acc_account_acc_username_uindex
    on acc_account (acc_username);



create table acr_account_roles
(
    acr_id        serial
        constraint acr_account_roles_pk
            primary key
        constraint fk1ub3jhqsp1xnjieb1khlpa0vw
            references acr_account_roles,
    acr_role_name varchar(255),
    acc_id        integer not null
        constraint fkh0lnvom8j9r9l4cftuu5dkoif
            references acc_account
);

alter table acr_account_roles
    owner to postgres;

create unique index acr_account_roles_acr_id_uindex
    on acr_account_roles (acr_id);


/*Password is 123456*/

/*Cliente*/
insert into acc_account (acc_id, acc_username, acc_password, acc_first_name, acc_last_name, acc_enable, acc_created_at,
                         acc_created_by, acc_modified_at, acc_modified_by)
values (nextval('acc_account_acc_id_seq'), 'joao@boasaude.com.br', '$2a$10$3LWe0v8rYnYQjr1/XGT.SuKNVBkSxDqksv72ia9RqfQqgZ7jebkWG', 'Joao', 'Raimundo', true, '2022-04-11', 'Admin', null, null);
insert into acr_account_roles (acr_id, acr_role_name, acc_id)

/*Prestador - Atendente*/
insert into acc_account (acc_id, acc_username, acc_password, acc_first_name, acc_last_name, acc_enable, acc_created_at,
                         acc_created_by, acc_modified_at, acc_modified_by)
values (nextval('acc_account_acc_id_seq'), 'atendimento@boasaude.com.br',
        '$2a$10$3LWe0v8rYnYQjr1/XGT.SuKNVBkSxDqksv72ia9RqfQqgZ7jebkWG', 'Carla', 'Atendimento', true, '2022-04-11',
        'Admin', null, null);

/*Prestador - Medico*/
insert into acc_account (acc_id, acc_username, acc_password, acc_first_name, acc_last_name, acc_enable, acc_created_at,
                         acc_created_by, acc_modified_at, acc_modified_by)
values (nextval('acc_account_acc_id_seq'), 'raimundo.medico@boasaude.com.br',
        '$2a$10$3LWe0v8rYnYQjr1/XGT.SuKNVBkSxDqksv72ia9RqfQqgZ7jebkWG', 'Raimundo', 'Medico', true, '2022-04-11',
        'Admin', null, null);


insert into acr_account_roles (acr_id, acr_role_name, acc_id)
values (nextval('acr_account_roles_acr_id_seq'), 'CLIENTE',
        (select a.acc_id from acc_account a where a.acc_username = 'joao@boasaude.com.br'));

insert into acr_account_roles (acr_id, acr_role_name, acc_id)
values (nextval('acr_account_roles_acr_id_seq'), 'ATENDIMENTO',
        (select a.acc_id from acc_account a where a.acc_username = 'atendimento@boasaude.com.br'));

insert into acr_account_roles (acr_id, acr_role_name, acc_id)
values (nextval('acr_account_roles_acr_id_seq'), 'MEDICO',
        (select a.acc_id from acc_account a where a.acc_username = 'raimundo.medico@boasaude.com.br'));