create database db_bs_atendimento
    with owner postgres;

comment on database db_bs_atendimento is 'Database Boa Saude Gestão de Atendimento';


create table ate_atendimento
(
    ate_id               serial
        constraint ate_atendimento_pk
            primary key,
    ate_prestador        varchar(255),
    ate_data_agendamento timestamp,
    ate_conveniado       varchar(255) not null,
    ate_associado        varchar(255) not null,
    ate_observacoes      varchar(1000),
    ate_confirmado       boolean,
    ate_data_criacao     timestamp    not null,
    ate_criado_por       varchar(255) not null,
    ate_data_atualizacao timestamp,
    ate_atualizado_por   varchar(255)
);

alter table ate_atendimento
    owner to postgres;

create table aex_exame
(
    aex_id               serial
        constraint ate_exame_pk
            primary key,
    aex_prestador        varchar(255),
    aex_data_agendamento timestamp,
    aex_conveniado       varchar(255) not null,
    aex_associado        varchar(255) not null,
    aex_observacoes      varchar(1000),
    aex_confirmado       boolean,
    aex_data_criacao     timestamp    not null,
    aex_criado_por       varchar(255) not null,
    aex_data_atualizacao timestamp,
    aex_atualizado_por   varchar(255),
    aex_tipo             varchar(255),
    ate_id               integer
        constraint aex_exame_ate_atendimento_ate_id_fk
            references ate_atendimento
);

alter table aex_exame
    owner to postgres;

create unique index aex_exame_aex_id_uindex
    on aex_exame (ate_id);

create unique index ate_atendimento_ate_id_uindex
    on ate_atendimento (ate_id);



